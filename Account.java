public class Account {
    
    private double balance;
    private String name;
   // private double value = 10;
	private static double interestRate = 10;

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

    public double addInterest(double balance) {
       return balance + (balance * interestRate) / 100;
        
    }

	public static double getInterestRate() {
		return interestRate;
	}

	public static void setInterestRate(double interestRate) {
		Account.interestRate = interestRate;
	}

	public boolean withdraw(double amount) {
		if (amount > balance) {
			System.out.println("Account transaction unsuccessful");
			return false;
		} else if (amount <= balance) {
			balance = balance - amount;
			System.out.println("Account transaction successful");
			return true;
		} else {
			return false;
		}
	}

	public boolean withdraw2() {
		if (balance >= 20) {
			balance = balance - 20;
			System.out.println("£20 has been deposited from your account");
			return true;
		} else if (balance < 20) {
			return false;
		} else {
			return false;
		}
	}

}