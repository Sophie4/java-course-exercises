public class TestAccount {
    
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23, 5444, 2, 345, 34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        
        for(int i =0; i< arrayOfAccounts.length; i++) {
            arrayOfAccounts[i] = new Account(names[i], amounts[i]);
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
            System.out.println("Account Name:" + " " + arrayOfAccounts[i].getName()); 
            System.out.println("Account Balance:" + " " + arrayOfAccounts[i].getBalance());
            System.out.println("New Balance after Interest added:" + " " +  arrayOfAccounts[i].addInterest(arrayOfAccounts[i].getBalance()));
            arrayOfAccounts[i].withdraw(40);
            System.out.println("New Account Balance:" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].withdraw2();
            System.out.print(System.lineSeparator());
        }
        Account myAccount = new Account("sophie", 40000);
        // myAccount.setBalance(400000);
        // myAccount.setName("Sophie");
        System.out.println("My Account:");
        System.out.println("Name:" + " " + myAccount.getName());
        System.out.println("Balance:" + " " + myAccount.getBalance());
        System.out.println("New Balance after Interest added:" + " " +  myAccount.addInterest(myAccount.getBalance()));
    } 
}